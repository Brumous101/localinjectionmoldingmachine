package injectionmoldingmachine;

import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.cinematic.MotionPath;
import com.jme3.cinematic.MotionPathListener;
import com.jme3.cinematic.events.MotionEvent;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;


/**
 * @author Kyle Johnson
 * @author Christopher McCormick
 */
public class Main extends SimpleApplication implements ActionListener {

    //setting up variables
    private MotionPath frontGatePath1, frontGatePath2;
    private MotionEvent frontGateMotionControl1, frontGateMotionControl2;
    private boolean frontGateOpen = false;
    private boolean frontGatePlaying = false; 
    private Node interactable, noninteractable;
    
    //setting up variables for physics
    private BulletAppState bulletAppState;
    private RigidBodyControl floorRigid, frontGateRigid, hopperRigid, moldsAndPlatesRigid, pelletRigid, baseRigid;
    private CharacterControl player;
    private Vector3f walkDirection = new Vector3f(), camDir = new Vector3f(), camLeft= new Vector3f();
    private boolean left = false, right = false, up = false, down = false;

    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }

    @Override
    public void simpleInitApp() {

        //set up physics
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        
        //set up user experience
        flyCam.setMoveSpeed(100);
        initHUD();
        initInputs();
        setUpKeys();

        //set up materials
        Material ssu1 = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        ssu1.setTexture("DiffuseMap", assetManager.loadTexture("Textures/ssu.png"));
        Material ssu2 = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        ssu2.setTexture("DiffuseMap", assetManager.loadTexture("Textures/ssu2.jpg"));
        Material floor1 = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        floor1.setTexture("DiffuseMap", assetManager.loadTexture("Textures/floor2.jpg"));
        Material blue1 = new Material(assetManager,"Common/MatDefs/Light/Lighting.j3md");
        blue1.setTexture("DiffuseMap", assetManager.loadTexture("Textures/blue1.jpg"));
        
        //create interactable spatials
        interactable = new Node("Interactable");
        rootNode.attachChild(interactable);
        makeFrontGate();

        //create non-interactable spatials
        noninteractable = new Node("Non-interactable");
        rootNode.attachChild(noninteractable);
        Objects obj = new Objects();
        Spatial hopper = assetManager.loadModel("Textures/IMM_Hopper.obj");
        obj.makeHopper(hopperRigid, noninteractable, bulletAppState, hopper);
        Spatial moldsAndPlates = assetManager.loadModel("Textures/Molds_And_Plates.obj");
        obj.makeMoldsAndPlates(moldsAndPlatesRigid, noninteractable, bulletAppState, moldsAndPlates);
        
        //create light - light is always pointing towards 0,0,0. makeLight's x, y, and z just move the position and angle.
        makeLight(-0.7f, -0.3f, -0.5f);
        makeLight(0.7f, -0.3f, -0.5f);
        makeLight(0.7f, -0.3f, 0.5f);
         
    //------------------------------------------------------------------------//
    //--------------------------BEGIN-PHYSICS---------------------------------//    
    //------------------------------------------------------------------------//    

        
        //create the floor
        Spatial floor = new Geometry("floor", new Box(25.0f, 0.5f, 25.0f));
        floor.setShadowMode(RenderQueue.ShadowMode.Receive);
        floor.setLocalTranslation(0.0f, -3.0f, 0.0f);
        floor.setMaterial(floor1);
        //create collision detection and collision shape for floor
        CollisionShape floorShape = CollisionShapeFactory.createMeshShape(floor);
        floorRigid = new RigidBodyControl(floorShape, 0);
        floor.addControl(floorRigid);
        noninteractable.attachChild(floor);
        bulletAppState.getPhysicsSpace().add(floorRigid);
        
        //create Base
        Spatial base = new Geometry("base", new Box(2.0f, 0.5f, 0.5f));
        base.setMaterial(blue1);
        base.setLocalTranslation(1.0f, -2.0f, 0.0f);
        noninteractable.attachChild(base);
        //create collision detection and collision shape for base
        CollisionShape baseShape = CollisionShapeFactory.createMeshShape(base);
        baseRigid = new RigidBodyControl(baseShape, 0);
        base.addControl(baseRigid);
        noninteractable.attachChild(base);
        bulletAppState.getPhysicsSpace().add(baseRigid);
        
        
        //create collision detection for the player
        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(1.5f, 1.5f, 1);  //originally (1.5f, 6f, 1)
        player = new CharacterControl(capsuleShape, 0.05f);
        player.setJumpSpeed(20);
        player.setFallSpeed(30);
        player.setGravity(new Vector3f(0,-30,0));
        player.setPhysicsLocation(new Vector3f(2.5f,1.0f,8.0f));
        bulletAppState.getPhysicsSpace().add(player);
        

        
        
    //------------------------------------------------------------------------//
    //----------------------------END-PHYSICS---------------------------------//    
    //------------------------------------------------------------------------//    
   

    //------------------------------------------------------------------------//

        // Forbid new motion of Front Gate until first motion is complete
        frontGatePath1.addListener( new MotionPathListener() {
            @Override
            public void onWayPointReach(MotionEvent control, int wayPointIndex) {
            if (frontGatePath1.getNbWayPoints() == wayPointIndex + 1) {
               frontGatePlaying = false;
                }
            }
        });
        frontGatePath2.addListener( new MotionPathListener() {
            @Override
            public void onWayPointReach(MotionEvent control, int wayPointIndex) {
            if (frontGatePath2.getNbWayPoints() == wayPointIndex + 1) {
               frontGatePlaying = false;
                }
            }
        });
        
    }

    //------------------------------------------------------------------------//
    
    //*
    protected Spatial makeFrontGate() {

        //create frontGate
        Spatial frontGate = assetManager.loadModel("Textures/Gate.obj");
        //frontGate.setMaterial(mat1);
        frontGate.setLocalTranslation(0.0f, -1.0f, 0.5f);
        frontGate.rotate(180 * FastMath.DEG_TO_RAD, 180 * FastMath.DEG_TO_RAD, 180 * FastMath.DEG_TO_RAD);
            
        CollisionShape frontGateShape = CollisionShapeFactory.createMeshShape(frontGate);
        frontGateRigid = new RigidBodyControl(0);
        frontGate.addControl(frontGateRigid);
        //frontGate.getControl(RigidBodyControl.class).setFriction(0.6f);
        interactable.attachChild(frontGate);
        bulletAppState.getPhysicsSpace().add(frontGateRigid);

        //------------------------------------------------------------------------//
        
        //frontGate OPEN
        frontGatePath1 = new MotionPath();
        frontGatePath1.addWayPoint(new Vector3f(0, -1, 0.5f));
        frontGatePath1.addWayPoint(new Vector3f(-1, -1, 0.5f));
            //path1.enableDebugShape(assetManager, rootNode);

        frontGateMotionControl1 = new MotionEvent(frontGate, frontGatePath1);
        frontGateMotionControl1.setDirectionType(MotionEvent.Direction.PathAndRotation);
        frontGateMotionControl1.setInitialDuration(10f);
        frontGateMotionControl1.setSpeed(15f);

        //frontGate CLOSE
        frontGatePath2 = new MotionPath();
        frontGatePath2.addWayPoint(new Vector3f(-1, -1, 0.5f));
        frontGatePath2.addWayPoint(new Vector3f(0, -1, 0.5f));

        frontGateMotionControl2 = new MotionEvent(frontGate, frontGatePath2);
        frontGateMotionControl2.setDirectionType(MotionEvent.Direction.PathAndRotation);
        frontGateMotionControl2.setInitialDuration(10f);
        frontGateMotionControl2.setSpeed(15f);
        
        return frontGate;

    }
    //*/
    //------------------------------------------------------------------------//

    
    
    protected Spatial makePlasticPellet(float num1, float num2, float num3) {

        //create Plastic Pellet
        Spatial pellet = assetManager.loadModel("Textures/Single_ABS_Pink.obj");
        pellet.setLocalTranslation(num1, num2, num3);
        pellet.setLocalScale(7.5f); //change size to view easier during development
        //pellet.rotate(90 * FastMath.DEG_TO_RAD,180 * FastMath.DEG_TO_RAD, 0f);
        
        CollisionShape pelletShape = CollisionShapeFactory.createMeshShape(pellet);
        pelletRigid = new RigidBodyControl(1.5f);
        pellet.addControl(pelletRigid);
        pellet.getControl(RigidBodyControl.class).setFriction(0.6f);
        noninteractable.attachChild(pellet);
        bulletAppState.getPhysicsSpace().add(pelletRigid);
        
        return pellet;
    }    
    
    //------------------------------------------------------------------------//
    
    protected DirectionalLight makeLight(float num1, float num2, float num3) {
        
            //create light
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection((new Vector3f(num1, num2, num3)).normalizeLocal());
        sun.setColor(ColorRGBA.White);
        rootNode.addLight(sun);
        return sun;

    }
    
    //------------------------------------------------------------------------//
        
    protected void initHUD() {

        //HUD - Basic Text explaining operation
        BitmapText hudText01 = new BitmapText(guiFont, false);
        hudText01.setSize(guiFont.getCharSet().getRenderedSize());
        hudText01.setColor(ColorRGBA.Black);
        hudText01.setText("Injection Molding Machine\n\nLeft click to interact.\n\nMovement:\nW: Forward\nS: Backward\nA: Left\nD: Right\nQ: Up\nZ: Down");
        hudText01.setLocalTranslation(300, hudText01.getLineHeight() + 250, 0);
        guiNode.attachChild(hudText01);

        //HUD - Crosshairs
        setDisplayStatView(false);
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText crosshairs = new BitmapText(guiFont, false);
        crosshairs.setSize(guiFont.getCharSet().getRenderedSize() * 2);
        crosshairs.setText("+"); // crosshairs
        crosshairs.setLocalTranslation(// center
                settings.getWidth() / 2 - crosshairs.getLineWidth() / 2, settings.getHeight() / 2 + crosshairs.getLineHeight() / 2, 0);
        guiNode.attachChild(crosshairs);

    }

    //------------------------------------------------------------------------//
    
    private void initInputs() {

        inputManager.addMapping("Interact", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));

        
        ActionListener acl = new ActionListener() {

            @Override
            public void onAction(String name, boolean keyPressed, float tpf) {

            
                if (name.equals("Interact") && !keyPressed) {
                    // Reset results list.
                    CollisionResults results = new CollisionResults();
                    // Aim the ray from camera location in camera direction
                    // (assuming crosshairs in center of screen).
                    Ray ray = new Ray(cam.getLocation(), cam.getDirection());
                    // Collect intersections between ray and all nodes in results list.
                    rootNode.collideWith(ray, results);
                    // Print the results so we see what is going on
                    for (int i = 0; i < results.size(); i++) {
                        // For each “hit”, we know distance, impact point, geometry.
                        float dist = results.getCollision(i).getDistance();
                        Vector3f pt = results.getCollision(i).getContactPoint();
                        String target = results.getCollision(i).getGeometry().getName();
                        System.out.println("Selection #" + i + ": " + target + " at " + pt + ", " + dist + " WU away.");
                    }
                    // 5. Use the results -- we rotate the selected geometry.
                    if (results.size() > 0) {
                        // The closest result is the target that the player picked:
                        Geometry target = results.getClosestCollision().getGeometry();

                        // Here comes the action:
                        
                        // Action for Front Gate
                        if (target.getName().equals("Gate-geom-0")) {

                            if (!frontGateOpen && !frontGatePlaying) {
                                frontGateOpen = true;
                                frontGateMotionControl1.play();
                                frontGatePlaying = true;
                            } else if(frontGateOpen && !frontGatePlaying) {
                                frontGateOpen = false;
                                frontGateMotionControl2.play();
                                frontGatePlaying = true;
                            }
                        }
                        // Example action for pouring pellets into the hopper
                        if (target.getName().equals("IMM_Hopper-geom-0")) {
                            noninteractable.attachChild(makePlasticPellet(2.0f, 1.0f, 0.0f));
                            noninteractable.attachChild(makePlasticPellet(2.1f, 1.0f, 0.0f));
                            noninteractable.attachChild(makePlasticPellet(2.1f, 1.2f, 0.1f));
                            noninteractable.attachChild(makePlasticPellet(1.9f, 1.0f, 0.0f));
                            noninteractable.attachChild(makePlasticPellet(1.9f, 1.2f, 0.1f));
                            noninteractable.attachChild(makePlasticPellet(2.0f, 1.0f, 0.1f));
                            noninteractable.attachChild(makePlasticPellet(2.1f, 1.0f, 0.1f));
                            noninteractable.attachChild(makePlasticPellet(2.1f, 1.2f, 0.2f));
                            noninteractable.attachChild(makePlasticPellet(1.9f, 1.0f, 0.1f));
                            noninteractable.attachChild(makePlasticPellet(1.9f, 1.2f, 0.2f));
                        }
                    }
                }
            }
        };

        inputManager.addListener(acl, "Interact");

    }
    
    private void setUpKeys() {
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addListener(this, "Left");
        inputManager.addListener(this, "Right");
        inputManager.addListener(this, "Up");
        inputManager.addListener(this, "Down");
        inputManager.addListener(this, "Jump");
    }
    
    @Override
    public void onAction(String binding, boolean isPressed, float tpf) {
        if (binding.equals("Left")) {
            left = isPressed;
        }
        else if (binding.equals("Right")) {
            right = isPressed;
        }
        else if (binding.equals("Up")) {
            up = isPressed;
        }
        else if (binding.equals("Down")) {
            down = isPressed;
        }
        else if (binding.equals("Jump")) {
            if (isPressed) { 
                player.jump(new Vector3f(0,10,0)); 
            }
        }
    }

    @Override
    public void simpleUpdate(float tpf) {
        camDir.set(cam.getDirection()).multLocal(0.3f);
        camLeft.set(cam.getLeft()).multLocal(0.2f);
        //camRight.set(cam.getLeft() ).multLocal(0.6f);
        walkDirection.set(0,0,0);
        if(left) {
            walkDirection.addLocal(camLeft);
        }
        if(right) {
            walkDirection.addLocal(camLeft.negate());
        }
        if(up) {
            walkDirection.addLocal(camDir);
        }
        if(down) {
            walkDirection.addLocal(camDir.negate());
        }
        player.setWalkDirection(walkDirection);
        cam.setLocation(player.getPhysicsLocation());
    }

    /*
    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
     */
}

//------------------------------------------------------------------------//
//                              UNUSED CODE                               //
//------------------------------------------------------------------------//

/*

        //create Box
        Box b = new Box(1, 1, 1);
        Spatial geom = new Geometry("Box", b);
        geom.setMaterial(ssu2);
        geom.setLocalTranslation(-2.0f, 0.0f, 0.0f);
        rootNode.attachChild(geom);


        //keymapping the keyboard
        inputManager.addMapping("door1", new KeyTrigger(KeyInput.KEY_O));
        //
                if (name.equals("door1") && keyPressed) {
                    if (!frontGateOpen) {
                        frontGateOpen = true;
                        frontGateMotionControl1.play();
                    } else {
                        frontGateOpen = false;
                        frontGateMotionControl2.play();
                    }
                }


        //adding shadow, needs tested at some point
        pellet.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);


        //example spatial with physics
        Spatial tempSpatial = assetManager.loadModel("Textures/Gate.obj");
        Material mat_default = new Material(assetManager, "Common/MatDefs/Misc/ShowNormals.j3md");
        tempSpatial.setMaterial(mat_default);
        tempSpatial.setLocalTranslation(-3.5f, 2.0f, 3.0f);
        noninteractable.attachChild(tempSpatial);
        //create collision detection and collision shape for tempSpatial
        CollisionShape sceneShape = CollisionShapeFactory.createMeshShape(tempSpatial);
        tempSpatialRigid = new RigidBodyControl(1);
        tempSpatial.addControl(tempSpatialRigid);
        tempSpatial.getControl(RigidBodyControl.class).setFriction(0.6f);
        bulletAppState.getPhysicsSpace().add(tempSpatialRigid);

 */